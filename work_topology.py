#!/usr/bin/python                                                                            

import networkx as nx
from mininet.net import Mininet
from mininet.topo import Topo
from mininet.cli import CLI

# G = nx.fast_gnp_random_graph(7,0.345)
# switch_candidates = {}
# switches = []
# for edge in G.edges:
#     if edge[0] in switch_candidates:
#         switch_candidates[edge[0]] += 1
#     else:
#         switch_candidates[edge[0]] = 1
#     if edge[1] in switch_candidates:
#         switch_candidates[edge[1]] +=1
#     else:
#         switch_candidates[edge[1]] = 1

# for candidate in switch_candidates:
#     if switch_candidates[candidate] > 1:
#         switches.append(candidate)

class RandomTopology( Topo ):
    def build(self, graph, switches_list):
        self.nodes_dict = {}
        self.new_switches = {}
        for node in graph.nodes:
            if node not in switches_list:
                self.nodes_dict[node] = self.addHost('h{0}'.format(node))
        for switch in switches_list:
            self.new_switches[switch] = self.addSwitch('s{0}'.format(switch))
        for edge in graph.edges:
            if edge[0] in self.nodes_dict:
                self.addLink(self.nodes_dict[edge[0]], self.new_switches[edge[1]])
            elif edge[0] in self.new_switches and edge[1] in self.nodes_dict:
                self.addLink(self.new_switches[edge[0]], self.nodes_dict[edge[1]])

def make_topo():
    G = nx.fast_gnp_random_graph(7,0.345)
    switch_candidates = {}
    switches = []
    for edge in G.edges:
        if edge[0] in switch_candidates:
            switch_candidates[edge[0]] += 1
        else:
            switch_candidates[edge[0]] = 1
        if edge[1] in switch_candidates:
            switch_candidates[edge[1]] +=1
        else:
            switch_candidates[edge[1]] = 1

    for candidate in switch_candidates:
        if switch_candidates[candidate] > 1:
            switches.append(candidate)
    print(switches)
    for node in G.nodes:
        if node not in switches:
            print(node)

    topology = RandomTopology(G, switches)
    print(type(topology))
    net = Mininet(topology)
    net.start()
    CLI(net)
    net.stop()


if __name__ == '__main__':
    make_topo()