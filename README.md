# Sdn-notes
## Openflow
### Openflow switch protocol
The Openflow protocol supports 3 types of messages:
* **Controller - to - switch** - messages are initiated by the contoller and used to directly manage or inspect the state of the switch.

### Openflow switch specification
You can find it [here](http://www.opennetworking.org/wp-content/uploads/2014/10/openflow-switch-v1.5.1.pdf).

### [Mininet python API guide](https://github.com/mininet/mininet/wiki/Introduction-to-Mininet)

